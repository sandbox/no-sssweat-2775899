(function($, Drupal, drupalSettings) {
  if (drupalSettings.machinename !== null) {
    $(document).bind('keydown', 'Ctrl+x', function(event) {
      setTimeout(function() {
	    var win = window.open('/admin/structure/types/manage/' + drupalSettings.machinename + '/fields', '_blank');
        if (win) {
          //Browser has allowed it to be opened
          win.focus();
        } 
	    else {
          //Browser has blocked it
          alert('PLEASE ALLOW POPUP FOR YOUR DEV SITE');
        }
      }, 0);
      return false;
    });
  }
})(jQuery, Drupal, drupalSettings);

(function ($) {
  $(document).bind('keydown', 'Ctrl+s', function(event) {
    setTimeout(function() {
  
      var submit = $('#edit-actions .button.js-form-submit.form-submit');
  
      if (submit.attr('value') != 'Search') {
        submit[0].click();
      }
      else if ($('.action-save.quickedit-button.icon')){
          $('.action-save.quickedit-button.icon')[0].click();	
      }
    }, 0);
    return false;
  });
}(jQuery));

(function($, Drupal, drupalSettings) {
  if (drupalSettings.machinename !== null) {
    $(document).bind('keydown', 'Ctrl+z', function(event) {
      setTimeout(function() {
	    var win = window.open('/node/add/' + drupalSettings.machinename);
        if (win) {
         //Browser has allowed it to be opened
         win.focus();
        } 
	    else {
          //Browser has blocked it
          alert('PLEASE ALLOW POPUP FOR YOUR DEV SITE');
        }
      }, 0);
      return false;
    });
  }
})(jQuery, Drupal, drupalSettings);

(function ($) {
  $(".contextual-region").mouseover(function(evt) {
    if (evt.ctrlKey) {
	  var test = $(this).children().children().next().children().find('a').html();
	  var test2 = $(this).children().children().next().children().next().find('a').html();
        if (test.indexOf('Edit') > -1 || test2.indexOf('Edit') > -1){
		  $(this).css({ "border": "solid green" });  
        }	
	}
 	if (evt.shiftKey) {
	  var test = $(this).children().children().next().children().find('a').html();
	    if (test == 'Configure block'){
		  $(this).css({ "border": "solid blue" });  
        }
		if (test == 'Quick edit'){
		  $(this).css({ "border": "solid #93e137" });  
        }			 
    }	  
	  
  });	

  $(".contextual-region").mouseout(function(evt) {
    $(this).css({ "border": "none" }); 
  });
	  
  $('.contextual-region').click(function(evt) {
    if (evt.ctrlKey) {
	  if ($('quickedit-field.quickedit-candidate.quickedit-editable:hidden')) {
        var test = $(this).children().children().next().children('li:nth-child(2)').find('a');
	      if (test.html() != 'Configure block'){
		    test[0].click(); 
		  }
      }
	}
	else if (evt.shiftKey) {
	  if ($('quickedit-field.quickedit-candidate.quickedit-editable:hidden')) {
        var test = $(this).children().children().next().children().find('a'); 
		  if (test.html() == 'Configure block' || 'Quick edit'){ 
		    test[0].click(); 			 
		  }
      }
	}
 });
}(jQuery));




